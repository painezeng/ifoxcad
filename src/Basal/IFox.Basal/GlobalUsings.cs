﻿// 系统引用
global using System;
global using System.Collections;
global using System.Collections.Generic;
global using System.IO;
global using System.Linq;
global using System.Text;
global using System.Reflection;
global using System.ComponentModel;
global using System.Runtime.InteropServices;
global using System.Diagnostics;
global using Microsoft.Win32;
global using System.Runtime.CompilerServices;
global using System.Threading;
global using System.Linq.Expressions;
global using System.Collections.ObjectModel;


#if NET45_OR_GREATER
global using Microsoft.CSharp.RuntimeBinder;
#endif